import RPi.GPIO as GPIO
import time
import os
import logging
import serial.tools.list_ports
import serial
import socket
import threading
from json import dumps, loads
import sys
from random import sample
import os.path
import re
from os import path
import paho.mqtt.client as paho


MODEM_CONTROL_PIN = 10

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(MODEM_CONTROL_PIN, GPIO.OUT)

CHECK_CONNECTIONS_PERIOD = 30

SEND_QUALITY_DATA_PERIOD = 60

GPS_START = 'AT+CGPS=1\r\n'.encode('utf-8')
GPS_STOP  = 'AT+CGPS=0\r\n'.encode('utf-8')
GPS_CHECK = 'AT+CGPS?\r\n'.encode('utf-8')

GSM_CHECK_NETWORK_QUALITY = 'AT+CSQ\r\n'.encode('utf-8')

networkQuality ={
    "rssi" : 0,
    "ber" : 0,
    "quality1" : 0,
    "quality2" : 0
}

DEFAULT_POLL_INTERVAL = 30
PUB_TOPIC = 't_networkQuality'
T_VERSION = 't_attributes'
VERSION = '0.1'  # vmarker
broker='localhost'
port=1883


def on_publish(client,userdata,result):             #create function for callback
    pass


clientNQ = paho.Client('networkQuality')
clientNQ.on_publish = on_publish  
clientNQ.connect(broker, port)
{"rtu-networkQuality-version": VERSION}
attributes = {"rtu-networkQuality-version": VERSION}
clientNQ.publish(T_VERSION, dumps(attributes))

# these are used to check if we have Internet or not
HOSTS = [('dns.google.com', 443),
         ('google.com', 443),
         ('microsoft.com', 443),
         ('ismyinternetworking.com', 443),
         ('rtec.dekart.com', 5672),  # RabbitMQ on rtec.dekart.com
         ('8.8.8.8', 443),  # google's regular DNS also serves ofer HTTPS
        ]

class Modem(object):
    def __init__(self):
        self.PRESENT_FLAG = False
        self.READY_LOG_FLAG = True
        self.modem_ready_timeout = 0     
        self.gps_on_count = 0
        self.GPS_STARTED_FLAG =  False

        self.ser = ''
        self.dialer_event = threading.Event()

        self.dialer_thread = threading.Thread(target=self.serve_dialer, name='TH_DIALER')
        self.dialer_thread.daemon = True

        self.gps_event = threading.Event()

        self.gps_thread = threading.Thread(target=self.control_gps, name='TH_GPS')
        self.gps_thread.daemon = True
        
        
        self.check_network_quality_event = threading.Event()

        self.check_network_quality_thread = threading.Thread(target=self.send_signal_quality, name='TH_CSQ')
        self.check_network_quality_thread.daemon = True


    def on_off(self):
        GPIO.output(MODEM_CONTROL_PIN, 1)
        time.sleep(1)
        GPIO.output(MODEM_CONTROL_PIN, 0)
        time.sleep(10)




    def present(self):
        if path.exists("/dev/modemAT"):
            return True
        else:
            return False


    def gps_state(self):
        self.ser.write(GPS_CHECK)
        res = self.ser.read(20)
        res = res.decode('utf-8')
        log.warning("response from function = {}".format(res))
        if res.find('+CGPS: 1') != -1:
            return True
        else:
            return False
    
    def AT(self):
        self.ser.write(b'AT\r\n')
        res = self.ser.read(20)
        res = res.decode('utf-8')
        if res.find('OK') != -1:
            return True
        else:
            return False

    def wait_ready(self):
        res = self.ser.read(20)
        res = res.decode('utf-8')
        if res.find('CALL READY') != -1:
            return True
        else:
            return False

    def internet_on(self):
        '''Check if we have an Internet connection by attempting
        to connect a TCP socket to a remote server'''
        hosts = sample(HOSTS, len(HOSTS))

        for entry in hosts:
            s = socket.socket()
            try:
                s.connect(entry)
            except socket.error as err:
                log.debug('Cannot connect to %s, %s', entry, err)
            else:
                # at least one of the target servers was
                # connectable, it means we're OK
                log.debug('%s is alive', entry)
                return True
            finally:
                # either way, we have to close the socket
                s.close()

        # if we got this far, it means we ran trough the entire
        # set of hosts to test with, and all were unreachable
        log.info('We appear to be offline')
        return False

    def serve_dialer(self):
        while True:
            self.dialer_event.wait()
            while not self.PRESENT_FLAG:
                if not self.present():
                    log.info("Modem is off, try to enable")
                    self.on_off()
                else:
                    log.info("Modem is already on")
                    self.ser = serial.Serial("/dev/modemAT", 115200, timeout=1, rtscts=True, dsrdtr=True)
                    if not self.AT():
                        log.warning("Modem is on, but do not response to AT command, try restart modem")
                        self.on_off()
                        continue
                    else:
                        log.info("Modem is OK !!!")
                    self.PRESENT_FLAG = True

            while not self.wait_ready():
                self.modem_ready_timeout += 1
                if self.modem_ready_timeout >= 60:
                    break

            if self.READY_LOG_FLAG:
                self.READY_LOG_FLAG = False
                log.info("CALL READY !!!")
                #this event release the dialer thread
            self.check_network_quality_event.set()
            log.info("Start wvdial !!!")
            os.system("sudo wvdial")
            time.sleep(CHECK_CONNECTIONS_PERIOD)
            
    def send_signal_quality(self):
        while True:
            signalQuall = "CSQ: 99,99"
            time.sleep(10)
            self.check_network_quality_event.wait()
            try:
                self.ser.write(GSM_CHECK_NETWORK_QUALITY)
                Quality_res = self.ser.read(60)
                Quality_res = Quality_res.decode('utf-8')
                
            except:
                log.warning("Can't acces port")
            
            signalQuall = substring_after(Quality_res, "CSQ: ")            
            signalQuallMess =  re.findall(r'\d+', signalQuall)
            
            updateParams("rssi", signalQuallMess[0])
            updateParams("ber", signalQuallMess[1])
            updateParams("quality1", convertRSSI(int(signalQuallMess[0])))
            updateParams("quality2", mapVal(int(signalQuallMess[0]),2,30,0,100))
            strData = dumps(networkQuality)
            
            log.warning("Quality response = {}".format(strData))
            
            ret = clientNQ.publish(PUB_TOPIC,strData)
            
            if ret[0] == 1:
                log.warning("Data not sent")
            else:
                log.warning("Data sent")
            
            #self.gps_event.set()
            time.sleep(SEND_QUALITY_DATA_PERIOD)

    def control_gps(self):
        while True:
            time.sleep(10)
            self.gps_event.wait()
            try:
                if self.internet_on():
                    if self.gps_state():
                        log.info ("GPS already started v1.1 !!!")
                    else:
                        self.GPS_STARTED_FLAG = False
                    while not self.GPS_STARTED_FLAG:
                        if self.gps_state():
                            log.info ("GPS already started !!!")
                            self.GPS_STARTED_FLAG = True
                            break
                        
                        try:
                            self.ser.reset_input_buffer()
                            self.ser.write(GPS_START)
                            res = self.ser.read(20)
                            res = res.decode('utf-8')
                            log.warning("response = {}".format(res))
                            if self.gps_state():
                                self.GPS_STARTED_FLAG = True
                                log.warning("GPS started successfully !!!")
                            
                            self.gps_on_count += 1
                            if self.gps_on_count >= 30:
                                self.gps_on_count = 0
                                log.warning("GPS cannot be started !!!")
                                break
                        except Exception as err:
                            log.exception('ERROR ---- %s', err)
                else:
                    self.GPS_STARTED_FLAG = False
            except:
                log.exception("ERROR")
            self.check_network_quality_event.set()
            time.sleep(60) 

    def start(self):
        log.info('Start dialer')
        self.dialer_thread.start()
        #self.gps_thread.start()
        self.check_network_quality_thread.start()

def substring_after(s, delim):
    return s.partition(delim)[2]

def updateParams (param,val):
    networkQuality[param] = val

def convertRSSI(rssi):
    if 2 <= rssi <= 9:
        return "Marginal"
    elif 10 <= rssi <= 14:
        return "OK"
    elif 15 <= rssi <= 19:
        return "Good"
    elif 20 <= rssi <= 30:
        return "Excelent"
    else:
        return "Unknown"

def mapVal(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

if __name__ == '__main__':

    log = logging.getLogger('Modem')
    logging.basicConfig(format='%(levelname)s - %(filename)s - %(funcName)s - %(message)s', level=logging.INFO)

    log.info("Hello from HELL 2.1")
    clientNQ = paho.Client("networkQuality")
    clientNQ.on_publish = on_publish  
    clientNQ.connect(broker,port)
    combo_modem = Modem()
    combo_modem.start()
    combo_modem.dialer_event.set()

    try:
        while True:
            try:
                time.sleep(1)

            except (KeyboardInterrupt, SystemExit): #when you press ctrl+c
                log.info('Quitting')
                break
        
    except Exception as err:
        # this must be a broad exception handler, to ensure that program will definitely
        # terminate when an anomaly occurs, thus allowing systemd to restart it. Otherwise
        # the process would get stuck forever, because some of its threads are alive and
        # others aren't
        log.exception('unexpected error %s', err)
        log.info('Quitting, systemd will restart me soon')
        os._exit(os.EX_UNAVAILABLE)